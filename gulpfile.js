// Enviroment: debug / production;
var env = "debug";

// Gulp.js configuration
var
  // modules
  gulp = require('gulp'),
  newer = require('gulp-newer'),
  imagemin = require('gulp-imagemin'),
  concat = require('gulp-concat'),
  deporder = require('gulp-deporder'),
  stripdebug = require('gulp-strip-debug'),
  uglify = require('gulp-uglify'),
  sassGlob = require('gulp-sass-glob');
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  assets = require('postcss-assets'),
  autoprefixer = require('autoprefixer'),
  cssnano = require('cssnano'),
  plumber = require('gulp-plumber'),

  // development mode?
  devBuild = (env !== 'production'),

  // folders
  folder = {
    src: 'src/',
    build: '_site/'
  }
;


// image processing
gulp.task('images', function() {
    var out = folder.build + 'assets/images/';
    return gulp.src(folder.src + 'assets/images/**/*')
      .pipe(newer(out))
      .pipe(imagemin({ optimizationLevel: 5 }))
      .pipe(gulp.dest(out));
});


gulp.task('html', function() {
    var
      out = folder.build,
      page = gulp.src(folder.src + '*.html')
        .pipe(newer(out));

    return page.pipe(gulp.dest(out));
});


// JavaScript processing
gulp.task('js', function() {

    var jsbuild = gulp.src(folder.src + 'assets/js/**/*')
      .pipe(plumber())
      .pipe(deporder())
      .pipe(concat('main.js'));

    if (!devBuild) {
      jsbuild = jsbuild
        .pipe(stripdebug())
        .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'assets/js/'));
  
});


// CSS processing
gulp.task('css', ['images'], function() {

    var postCssOpts = [
    assets({ loadPaths: ['assets/images/'] }),
    autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
    cssnano
    ];
  
    return gulp.src(folder.src + 'assets/scss/main.scss')
      .pipe(plumber())
      .pipe(sassGlob())
      .pipe(sass({
        outputStyle: 'nested',
        imagePath: 'assets/images/',
        precision: 3,
        errLogToConsole: true
      }))
      .pipe(postcss(postCssOpts))
      .pipe(gulp.dest(folder.build + 'assets/css/'));
  
});


// run all tasks
gulp.task('run', ['css', 'js', 'html']);


// watch for changes
gulp.task('watch', function() {

    // image changes
    gulp.watch(folder.src + 'assets/images/**/*', ['images']);
  
    // javascript changes
    gulp.watch(folder.src + 'assets/js/**/*', ['js']);
  
    // css changes
    gulp.watch(folder.src + 'assets/scss/**/*', ['css']);

    // html changes
    gulp.watch(folder.src + '*.html', ['html']);
  
});


// default task
gulp.task('default', ['run', 'watch']);