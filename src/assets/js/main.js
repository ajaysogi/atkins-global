"use strict";

// $(".carasouel").slick({
//     "arrows": false
// });
$(document).ready(function(){

    

    $('#menuToggle').on('click', function(){
        $(this).toggleClass('open');
        $('.main-nav').toggleClass('show');

        if ( $('.main-nav').hasClass('show') ) {
            $('.main-nav').find('li').addClass('animated fadeInLeft');
        } else {
            $('.main-nav').find('li').removeClass('animated fadeInLeft');
        }
    });


    // Wait all background images to load before calling the slider
    $('.carasouel')
        .imagesLoaded( { background: '.carasouel-slide' }, function() {
            console.log('all .item background images loaded');
        })
        .done( function( instance ) {
            
            var slides = $('.carasouel-content');
            var $slider = $('.carasouel')
                .on('init', function(slick) {
                    slides.first().addClass('animated fadeInLeft faster');
                })
                .slick({
                    focusOnSelect: true,
                    speed: 1000,
                    "arrows": false,
                    autoplay: true,
                    autoplaySpeed: 3000,
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                
                })
                .on('afterChange', function(event, slick, currentSlide) {
                    var activeSlide = $('.carasouel-slide.slick-active').find('.carasouel-content');
                    slides.not(activeSlide).removeClass('animated fadeInLeft faster');
                    activeSlide.addClass('animated fadeInLeft faster');
                });

        })
        .progress( function( instance, image ) {
            var result = image.isLoaded ? 'loaded' : 'broken';
            console.log( 'image is ' + result + ' for ' + image.img.src );
        });


});

    


