"use strict";

// $(".carasouel").slick({
//     "arrows": false
// });
$(document).ready(function(){

    

    $('#menuToggle').on('click', function(){
        $(this).toggleClass('open');
        $('.main-nav').toggleClass('show');

        if ( $('.main-nav').hasClass('show') ) {
            $('.main-nav').find('li').addClass('animated fadeInLeft');
        } else {
            $('.main-nav').find('li').removeClass('animated fadeInLeft');
        }
    });


    // Wait all background images to load before calling the slider
    $('.carasouel')
        .imagesLoaded( { background: '.carasouel-slide' }, function() {
            
        })
        .done( function( instance ) {
            
            // Initalize Slider

            var slides = $('.carasouel-content');
            var $slider = $('.carasouel')
                .on('init', function(slick) {
                    slides.first().addClass('animated fadeInLeft faster');
                })
                .slick({
                    focusOnSelect: true,
                    speed: 1000,
                    "arrows": false,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    pauseOnHover:false
                })
                .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                
                })
                .on('afterChange', function(event, slick, currentSlide) {
                    var activeSlide = $('.carasouel-slide.slick-active').find('.carasouel-content');
                    slides.not(activeSlide).removeClass('animated fadeInLeft faster');
                    activeSlide.addClass('animated fadeInLeft faster');
                })
                .slick('slickPause');

            
           

        })
        .progress( function( instance, image ) {

        });


});


$(window).on("load", function(){
    // Remove Preloader (Delay is only for demo puroses)
    $('#preloader').delay('1200').queue(function(next){
        $(this).addClass('animated bounceOutDown');
        $('.carasouel').slick('slickPlay');
        next();
    });

});
    


